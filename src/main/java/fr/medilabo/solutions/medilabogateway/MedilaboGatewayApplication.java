package fr.medilabo.solutions.medilabogateway;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import org.springframework.web.reactive.function.client.WebClient;

import fr.medilabo.solutions.medilabogateway.filters.AuthenticationFilter;

@SpringBootApplication
public class MedilaboGatewayApplication {

	@Value("${authentication.baseuri}")
	private String authenticationBaseUri;

	@Value("${patients.baseuri}")
	private String patientsBaseUri;

	@Value("${notes.baseuri}")
	private String notesBaseUri;

	@Value("${diabete.baseuri}")
	private String diabeteBaseUri;

	public static void main(String[] args) {
		SpringApplication.run(MedilaboGatewayApplication.class, args);
	}

	@Bean
	public RouteLocator routeLocator(RouteLocatorBuilder builder, AuthenticationFilter authenticationFilter) {
		return builder.routes()
				.route(r -> r
						.path("/auth/**")
						.uri(authenticationBaseUri))
				.route(r -> r
						.path("/patient/**")
						.filters(f -> f.filter(authenticationFilter))
						.uri(patientsBaseUri))
				.route(r -> r
						.path("/note/**")
						.filters(f -> f.filter(authenticationFilter))
						.uri(notesBaseUri))
				.route(r -> r
						.path("/diabete/**")
						.filters(f -> f.filter(authenticationFilter))
						.uri(diabeteBaseUri))
				.build();
	}

	@Bean
	public AuthenticationFilter authenticationFilter(@Value("${authentication.baseuri}") String authenticationUrl) {
		WebClient authClient = WebClient.builder().baseUrl(authenticationUrl).build();
		return new AuthenticationFilter(authClient);
	}

	@Bean
	public CorsWebFilter corsFilter() {
		CorsConfiguration corsConfiguration = new CorsConfiguration();
		corsConfiguration.setAllowCredentials(true);
		corsConfiguration.addAllowedOrigin("http://localhost:4200");
		corsConfiguration.addAllowedOrigin("http://localhost:80");
		corsConfiguration.addAllowedOrigin("http://localhost");
		corsConfiguration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS", "HEAD"));
		corsConfiguration.addAllowedHeader("*");

		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", corsConfiguration);

		return new CorsWebFilter(source);
	}
}
