package fr.medilabo.solutions.medilabogateway.filters;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ServerWebExchange;

import reactor.core.publisher.Mono;

public class AuthenticationFilter implements GatewayFilter {

    private WebClient authenticationClient;

    public AuthenticationFilter(WebClient authenticationClient) {
        this.authenticationClient = authenticationClient;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        return authenticationClient.get()
                .uri("/auth/tokeninfo")
                .headers(httpHeaders -> httpHeaders.putAll(exchange.getRequest().getHeaders()))
                .retrieve()
                .onStatus(s -> s.is2xxSuccessful(), response -> chain.filter(exchange).then(Mono.empty()))
                .onStatus(s -> !s.is2xxSuccessful(), r -> {
                    ServerHttpResponse response = exchange.getResponse();
                    response.setStatusCode(HttpStatus.UNAUTHORIZED);
                    return response.setComplete().then(Mono.empty());
                })
                .bodyToMono(Void.class);
    }

}
