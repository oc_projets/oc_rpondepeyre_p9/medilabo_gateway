
# --------Build------------

FROM maven:3.9.4-amazoncorretto-20 AS builder
WORKDIR /app
COPY . .
RUN mvn package

# ---------RUN-----------

FROM amazoncorretto:20
WORKDIR /app
COPY  --from=builder /app/target/MedilaboGateway-1.0.0.jar .

CMD [ "java", "-jar","MedilaboGateway-1.0.0.jar"]